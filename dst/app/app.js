"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createApp = void 0;
const express = require("express");
// import  router from './routes';
// import { MongoClient } from "mongodb";
// import { createDbConnection } from '../mongoDB';
const apollo_server_express_1 = require("apollo-server-express");
const mongoDB_1 = require("../mongoDB");
const cors = {
    origin: '*',
    // allowedHeaders: [
    //   'Origin',
    //   'X-Requested-With',
    //   'Content-Type',
    //   'Accept',
    //   'X-Access-Token',
    //   'Authorization',
    // ],
    // methods: ['GET', 'HEAD', 'OPTIONS', 'PUT', 'PATCH', 'POST', 'DELETE'],
    // optionsSuccessStatus: 200,
    credentials: true,
};
// // app.use(passport.initialize());
// app.use(cors(opts));
// app.use(
//   express.json({
//     limit: '1mb',
//   })
// );
// app.use(
//   express.urlencoded({
//     limit: '1mb',
//     extended: true,
//   })
// );
// app.get('/check', (_req, res) => {
//   res.json('allow api serve - test runner');
// });
// // app.use('/v1/auth/', authRoute);
// // passport.use(strategy);
// // app.use('/v1/', jwtAuthenticate, router);
// app.use('/v1/', router);
// let dbConnection: MongoClient | undefined;
// export async function createApp() {
//   if (!dbConnection) {
//     dbConnection = await createDbConnection();
//   }
// }
let dbConnection;
function createApp() {
    return __awaiter(this, void 0, void 0, function* () {
        console.log("DB connecting...");
        if (!dbConnection) {
            dbConnection = yield mongoDB_1.createDbConnection();
        }
        console.log("DB connected");
        const typeDefs = apollo_server_express_1.gql `
    # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

    # This "Book" type defines the queryable fields for every book in our data source.
    type Book {
      title: String
      author: String
    }

    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each. In this
    # case, the "books" query returns an array of zero or more Books (defined above).
    type Query {
      books: Book
    }
  `;
        const books = () => __awaiter(this, void 0, void 0, function* () {
            const blog = yield dbConnection.db('ecommerce').collection('blog').findOne({ title: 'name' });
            return {
                title: blog.title,
                author: blog.author
            };
        });
        const resolvers = {
            Query: {
                books: () => books(),
            },
        };
        const app = express();
        const server = new apollo_server_express_1.ApolloServer({
            typeDefs,
            resolvers,
        });
        server.start();
        server.applyMiddleware({ app, cors, path: '/graphql' });
        return app;
    });
}
exports.createApp = createApp;
//# sourceMappingURL=app.js.map