"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_1 = require("http-status");
class BaseController {
    constructor(table) {
        this.table = table;
    }
    created(res, result) {
        if (result === undefined) {
            res.status(http_status_1.CREATED).send();
        }
        else {
            res.status(http_status_1.CREATED).json(result);
        }
    }
    nextWapper(mainFunction) {
        return (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield mainFunction(req, res, next);
            }
            catch (err) {
                next(err);
            }
        });
    }
}
exports.default = BaseController;
//# sourceMappingURL=baseController.js.map