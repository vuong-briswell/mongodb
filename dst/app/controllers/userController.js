"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseController_1 = require("./baseController");
const domain_1 = require("../../domain");
const mongoDB_1 = require("../../mongoDB");
class UserController extends baseController_1.default {
    constructor(table) {
        super(table);
        this.create = (req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            yield this.userRep.create(req.body);
            this.created(res);
        });
        this.find = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const find = yield this.userRep.find();
            res.json(find);
        });
        this.work = (req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const up = yield this.userRep.work(req.body);
            this.created(res, up);
        });
        this.join = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const join = yield this.userRep.join();
            res.json(join);
        });
        this.listWork = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const work = yield this.userRep.listWork();
            res.json(work);
        });
        this.listTypeWork = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const typeWork = yield this.userRep.findTypeWork();
            res.json(typeWork);
        });
        this.transaction = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const tran = yield this.userRep.transactionLearning();
            res.json(tran);
        });
        this.learnMoreAboutJoin = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const join = yield this.userRep.learnMoreAboutJoin();
            res.json(join);
        });
        this.createWeek = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const week = yield this.userRep.createWeek();
            res.json(week);
        });
        this.selectWeek = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const week = yield this.userRep.switch();
            res.json(week);
        });
        this.facet = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const week = yield this.userRep.facet();
            res.json(week);
        });
        this.bucket = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const week = yield this.userRep.bucket();
            res.json(week);
        });
        this.addFields = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const field = yield this.userRep.addFields();
            res.json(field);
        });
        this.graphLookup = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const field = yield this.userRep.graphLookup();
            res.json(field);
        });
        this.createDate = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const date = yield this.userRep.createDate();
            res.json(date);
        });
        this.listDate = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const list = yield this.userRep.listDate();
            res.json(list);
        });
        this.search = (req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const list = yield this.userRep.searchConditions(req.query);
            res.json(list);
        });
        this.populate = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const list = yield this.userRep.populate();
            res.json(list);
        });
        this.document = (_req, res, _next) => __awaiter(this, void 0, void 0, function* () {
            const list = yield this.userRep.document();
            res.json(list);
        });
        this.userRep = new domain_1.repository.Users(this.table);
        // this.get = this.nextWapper(this.get);
        this.transaction = this.nextWapper(this.transaction);
        this.document = this.nextWapper(this.document);
    }
}
exports.default = new UserController(mongoDB_1.default);
//# sourceMappingURL=userController.js.map