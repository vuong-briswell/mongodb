"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Blog = void 0;
const mongoose_1 = require("mongoose");
const _common_1 = require("./_common");
const Blog = (mongoose) => {
    const BlogSchema = new mongoose.Schema({
        // customerId: {
        //     type: Schema.Types.ObjectId,
        //     ref: 'Customer',
        //     required: true
        // },
        title: {
            type: mongoose_1.Schema.Types.String,
        },
        content: {
            type: mongoose_1.Schema.Types.String,
        },
        type: {
            type: String,
            enum: ['php', 'nodejs', 'reactjs'],
        }
    }, {
        toJSON: {
            virtuals: true
        },
        id: true,
        versionKey: false
    }).plugin(_common_1.timestamp);
    return mongoose.model('blog', BlogSchema, 'blog');
};
exports.Blog = Blog;
//# sourceMappingURL=Blog.js.map