"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.timestamp = void 0;
// import { types } from '../../factory';
const mongoose_1 = require("mongoose");
const timestamp = (schema) => {
    schema.add({
        created_at: {
            type: Date,
            default: Date.now()
        },
        created_by: mongoose_1.Types.ObjectId,
        updated_at: {
            type: Date,
            default: Date.now()
        },
        updated_by: mongoose_1.Types.ObjectId,
        deleted_at: Date,
        deleted_by: mongoose_1.Types.ObjectId
    });
    // schema.pre<types.Common.FieldCM & any>('save', (next: HookNextFunction) => {
    //     next();
    // });
    // schema.post('save', (next: HookNextFunction) => {
    //     next();
    // })
};
exports.timestamp = timestamp;
//# sourceMappingURL=_common.js.map