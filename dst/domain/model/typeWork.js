"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypeWork = void 0;
const mongoose_1 = require("mongoose");
const _common_1 = require("./_common");
class TypeWork extends mongoose_1.Schema {
}
exports.TypeWork = TypeWork;
const work = new TypeWork({
    name_type: {
        type: String
    }
}).plugin(_common_1.timestamp);
exports.default = mongoose_1.model('TypeWork', work, 'typeWork');
//# sourceMappingURL=typeWork.js.map