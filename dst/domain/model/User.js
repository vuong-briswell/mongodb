"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const mongoose_1 = require("mongoose");
const _common_1 = require("./_common");
const User = (mongoose) => {
    const UserSchema = new mongoose.Schema({
        email: {
            type: mongoose_1.Schema.Types.String,
            required: true
        },
        name: {
            type: mongoose_1.Schema.Types.String,
            required: true
        },
        password: {
            type: mongoose_1.Schema.Types.String,
        },
        token: {
            type: mongoose_1.Schema.Types.String,
        },
        expire: {
            type: mongoose_1.Schema.Types.Date,
            required: true
        }
    }, {
        toJSON: {
            virtuals: true
        },
        id: true,
        versionKey: false
    }).plugin(_common_1.timestamp);
    return mongoose.model('user', UserSchema, 'user');
};
exports.User = User;
//# sourceMappingURL=User.js.map