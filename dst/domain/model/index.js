"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initialize = void 0;
// import  Work from './work';
// import TypeWork from './TypeBlog';
// import Week from './week';
// import Date from './date';
const Blog_1 = require("./Blog");
const User_1 = require("./User");
const initialize = (mongoose) => {
    const model = {
        Blog: Blog_1.Blog(mongoose),
        User: User_1.User(mongoose)
    };
    return Object.assign(Object.assign({}, model), { mongoose });
};
exports.initialize = initialize;
//# sourceMappingURL=index.js.map