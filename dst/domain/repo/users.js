// import BaseRepository from './_base';
// import { Table } from '../model';
// import * as db from 'mongoose';
// import { types } from '../../factory';
// export default class AuthRepository extends BaseRepository {
//     private readonly model: Table['User']
//     private readonly modelWork: Table['Work']
//     private readonly modelTypeWork: Table['TypeWork']
//     private readonly modelWeek: Table['Week']
//     private readonly modelDate: Table['Date']
//     constructor(table: Table) {
//         super(table);
//         this.model = table.User;
//         this.modelWork = table.Work;
//         this.modelTypeWork = table.TypeWork;
//         this.modelWeek = table.Week;
//         this.modelDate = table.Date;
//     }
//     public create = async(data: any) => {
//         // if (data.name_work) {
//         //     const modelUser = new this.modelWork();
//         //     modelUser.name_work = data.name_work;
//         //     await modelUser.save();   
//         // }
//         // const modelUser = new this.model();
//         // modelUser.username = data.username;
//         // modelUser.password = data.password;
//         // modelUser.email = data.email;
//         // await modelUser.save();
//         if (data.name_type) {
//             const modelTW = new this.modelTypeWork();
//             modelTW.name_type = data.name_type;
//             await modelTW.save();
//             // modelTW.$ignore
//             await this.modelWork.findByIdAndUpdate('5fd877d76a1d9d34fc5df65d', {
//                 _type_work: modelTW._id
//             })
//         }
//     }
//     public work = async(data: any) => {
//         const modelWork = new this.modelWork();
//         modelWork.name_work = data.name_work;
//         modelWork._type_work = data._type_work;
//         await modelWork.save();
//         const up = await this.model.findByIdAndUpdate(data.id_user,
//         {
//             $addToSet: {
//                 // '_work': {
//                 //     $each: data.id_work
//                 // },
//                 '_work':modelWork._id
//             }
//         }
//         );
//         return up;
//     }
//     public findTypeWork = async() => {
//         return await this.modelTypeWork.find();
//     }
//     public queries = async () => {
//        const user = await this.model.find({}, {
//            '_id': false
//        }).select('username password email').where('username').in(['asd', 'hihi']).exec();
//        this.model.findByIdAndDelete();
//         this.model.deleteMany({
//             email: 'vuong@gmail.com'
//         });
//         this.model.deleteOne({
//             email: 'vuong@gmail.com'
//         });
//         // input is id
//         this.model.findById('1');
//         //
//         this.model.findByIdAndDelete('id', {
//         });
//         this.model.findByIdAndRemove('id', {
//             sort: '', // if found multiple docs . maybe set sort to choose which doc to update
//             rawResult: false,
//             strict: true // overwrites the schema's strict mode option for this update
//             // options
//         });
//         this.model.findByIdAndUpdate();
//         this.model.findOne();
//         this.model.findOneAndDelete();
//         this.model.findOneAndRemove();
//         this.model.findOneAndReplace();
//         this.model.findOneAndUpdate();
//         this.model.replaceOne();
//         this.model.updateMany();
//         this.model.updateOne();
//         return user;
//     }
//     public find = async () => {
//         const user = await this.model.find();
//         return user;
//     }
//     public document = async () => {
//         // const session = await this.modelWork.startSession();
//         // const modelUser = new this.modelWork();
//         // await this.modelWork.findByIdAndUpdate('5fd9a109093fe937549c0f14',{
//         //     name_work: 'coffee',
//         // });
//         // modelUser.name_work = 'coffee'
//         // modelUser.$session(session);
//         // modelUser.$ignore('name_work'); ignore validate of name_work
//         // const err = modelUser.validateSync();
//         // modelUser.save();
//         // assert.strictEqual(err!.message, 'Too few eggs');
//         // await modelUser.save();
//         // return await this.modelWork.find()
//         // return null;
//         // return await this.modelWork.find();
//         const date = new this.modelDate;
//         return this.modelDate.myStatic();
//     }
//     public query = async () => {
//         const query = this.model.find();
//         query.$where( (we: any) => {
//             console.log(we);
//         })
//     }
//     public aggregate = async() => {
//         const aggregate = this.model.aggregate([
//             {
//                 $match: {
//                     username: {
//                         $cond: {
//                             if: { $eq: ["$email", "aaaaaaa@gmail.vvv"] },
//                             then: '123',
//                             else: 'asd'
//                         }
//                     }
//                 },
//             },
//             {
//                 $project: {
//                     "_id": false,
//                     username: true,
//                     email: true,
//                     tong_hop: {
//                         $concat: [
//                             "$username", "-", "$email",
//                             {
//                                 $cond: {
//                                     if: { $eq: ["$email", "aaaaaaa@gmail.vvv"] },
//                                     then: "email not good",
//                                     else: "email good"
//                                 }
//                             }, "-nothing"
//                         ]
//                     }
//                 },
//             }
//           ]);
//           return await aggregate;
//     }
//     public join = async() => {
//         const join = await this.model.aggregate([
//             {
//                 $lookup: {
//                     from: 'work',
//                     localField: '_work',
//                     foreignField: '_id',
//                     as: 'workList',
//                 }
//             },
//             {
//                 $unwind: {
//                     path: "$workList",
//                     preserveNullAndEmptyArrays: true
//                 }
//             },
//             {
//                 $lookup: {
//                     from: "typeWork",
//                     localField: 'workList._type_work',
//                     foreignField: '_id',
//                     as: 'workList.workType',
//                 }
//             },
//             // {
//             //     $group: {
//             //         _id: "$_id",
//             //         username: { $first: "$username" },
//             //         password: { $first: "$password" },
//             //         email: { $first: "$email" },
//             //         _work: { $first: "$_work" },
//             //         workList: {
//             //             $push: {
//             //                 $cond: {
//             //                     if: { $ne: [ "$_work", []]  },
//             //                     then: NaN,
//             //                     else: 'aaaaa'
//             //                 },
//             //             }
//             //         }
//             //     }
//             // },
//             {
//                 $group: {
//                     _id: "$_id",
//                     _work: { $first: "$_work" },
//                     username: { $first: "$username" },
//                     password: { $first: "$password" },
//                     email: { $first: "$email" },
//                     workList: {
//                         // $addToSet:{
//                         //     _id: "$workList._id",
//                         //     name_work: "$workList.name_work",
//                         // },
//                         // $push: {
//                         //     $cond: {
//                         //         if: { "$workList._id": { $exists: true }},
//                         //         then: null,
//                         //         else: 'ok k null'
//                         //     },
//                         // }
//                     //}
//                     // countWL: {
//                     //     $sum: {
//                     //         $size: "$workList"
//                     //     }
//                     // },
//                     // workList: {
//                     //     // checkWorkList: {
//                             // $cond: {
//                             //     if: { $eq: ["$workList", []] },
//                             //     then: null,
//                             //     else: 'ok k null'
//                             // },
//                     //     // },
//                         $push: {
//                            _id: "$workList._id",
//                            name_work: "$workList.name_work",
//                             // workType: "$workList.workType", -> ok
//                             workType: {
//                             // $arrayElemAt: ["$workList.workType", 0] -> ok
//                                 // $cond: { if: { $eq: ["$workList.name_work", "ruby"] }, then: true, else: false }
//                                 // $ifNull: [ "$workList.workType", { -> nếu khác null thì hiển thị
//                                 //     $arrayElemAt: ["$workList.workType", 0] -> nếu là null thì hiển thị
//                                 // } ]
//                                 $cond: {
//                                     if: { $eq: ["$workList.workType", []] },
//                                     then: null,
//                                     else: {
//                                         $arrayElemAt: ["$workList.workType", 0]
//                                     }
//                                 }
//                             },
//                             // count: {
//                             //     $sum: {
//                             //         $size: "$workList.workType"
//                             //     }
//                             // }
//                         },
//                         // _id: "$workList._id",
//                         // name_work: "$workList.name_work",
//                         // workType: {
//                         //     $arrayElemAt: ["$workType", 0]
//                         // }
//                     },
//                 }
//             },
//             {
//                 $project: {
//                     username: "$username",
//                     password: "$password",
//                     email: "$email",
//                     workList: {
//                         $cond: {
//                             if: { $eq: [{ $size: "$_work" }, 0] },
//                             then: [],
//                             else: "$workList"
//                         }
//                     },
//                     // count: {
//                     //     $cond: {
//                     //         if: { $eq: [{ $size: "$_work" }, 0] },
//                     //         then: 'ok o',
//                     //         else: 'not o'
//                     //     }
//                     // },
//                 }
//                 // $project: {
//                 //     // "__v": false,
//                 //     // "workList.workType.__v": false,
//                 //     // "workT": {
//                 //     //     $arrayElemAt: [ "$workType", 0 ]
//                 //     // }
//                 //     // count: {
//                 //     //     $size: "$_work"
//                 //     // },
//                 //     // "_work": false,
//                 //     // workList: {
//                 //     //     $cond: {
//                 //     //         if: { $eq: [{ $size: "$_work" }, 0] },
//                 //     //         then: [],
//                 //     //         else:"$workList"
//                 //     //     }
//                 //     // }
//                 // }
//             },
//             // {
//             //     // $match: {
//             //     //     // username: 'a987'
//             //     // }
//             // }
//             // {
//             //     $group: {
//             //         _id: "$_id",
//             //         username: {$first: "$username"},
//             //         email: {$first: "$email"},
//             //         workList: {
//             //             $push: {
//             //                 name_work: "$workList.name_work",
//             //                 workType: {
//             //                     $arrayElemAt: ["$workType", 0]
//             //                 }
//             //             }
//             //         }
//             //     }
//             // }
//             // {
//             //     $unwind: {
//             //         path: "$workType",
//             //         preserveNullAndEmptyArrays: true
//             //     }
//             // }
//         ]);
//         return join;
//     }
//     public listWork = async () => {
//         const list = await this.modelWork.find();
//         return list;
//     }
//     public transactionLearning = async () => {
//         const session = await db.startSession();
//         session.startTransaction();
//         try {
//             // await this.model.findByIdAndUpdate('5fd2ed556bb01e42c885f585', {
//             //     username: 'transaction'
//             // }, { session });
//             await this.model.create([
//                 {
//                     username: 'tran-create',
//                     email: 'email-tran@gmail.com',
//                     password: '213',
//                 }
//             ], { session });
//             await session.commitTransaction();
//             return await this.model.find();
//         } catch(err) {
//             await session.abortTransaction();
//             throw err;
//         } finally {
//             session.endSession();
//         }
//     }
//     public async learnMoreAboutJoin() {
//         const result = await this.model.aggregate([
//             {
//                 $lookup: {
//                     from: 'work',
//                     // localField: '_work',
//                     // foreignField: '_id',
//                     let: {work_id: '$_work'},
//                     pipeline: [
//                         {
//                            $match: {
//                                $expr: {
//                                    $and: [
//                                        { $in: ["$_id", "$$work_id"] }
//                                    ]
//                                }
//                            }
//                         },
//                         {
//                             // $lookup: {
//                             //     from: "typeWork",
//                             //     localField: '_type_work',     => như này ok
//                             //     foreignField: '_id',
//                             //     as: 'data_workType',
//                             // }
//                             // case else
//                             $lookup: {
//                                 from: "typeWork",
//                                 let: { id_type_work: "$_type_work" },
//                                 pipeline: [
//                                     {
//                                         $match: {
//                                             $expr: {
//                                                 $and: [
//                                                     { $eq: ["$_id", "$$id_type_work"] }
//                                                 ]
//                                             }
//                                         }
//                                     },
//                                     {
//                                         $project: {
//                                             _id: true,
//                                             name_type: true
//                                         }
//                                     }
//                                 ],
//                                 as: 'data_workType',
//                             },
//                         },
//                         {
//                             $project: {
//                                 "_id": true,
//                                 "name_work": true,
//                                 "_type_work": true,
//                                 data_workType: {
//                                     $cond: {
//                                         if: { $eq: [{ $size: "$data_workType" }, 0] },
//                                         then: {},
//                                         else: {
//                                             $arrayElemAt: ["$data_workType", 0]
//                                         }
//                                     }
//                                 }
//                             }
//                         }
//                     ],
//                     as: 'workList',
//                 }
//             },
//             // {
//             //     $unwind: {
//             //         path: "$workList",
//             //         preserveNullAndEmptyArrays: true
//             //     }
//             // },
//             // {
//             //     $lookup: {
//             //         from: "typeWork",
//             //         localField: 'workList._type_work',
//             //         foreignField: '_id',
//             //         as: 'workType',
//             //     }
//             // },
//         ]);
//         return result;
//     }
//     public async switch() {
//         // sort = 1 tăng dần
//         // sort = -1 giảm dần
//         // custom sort { $meta: "textScore" }
//         const weekSelect = await this.modelWeek.aggregate([
//             {
//                 $sort: { day_work: -1 }
//             },
//             { $limit: 100 },
//             { $skip: 1 },
//             {
//                 $project: {
//                     _id: true,
//                     day_of_week: {
//                         $switch: {
//                             branches: [
//                                 { case: { $eq: ["$day_work", 0] }, then: "Sunday" },
//                                 { case: { $eq: ["$day_work", 1] }, then: "Monday" },
//                                 { case: { $eq: ["$day_work", 2] }, then: "Tuesday" },
//                                 { case: { $eq: ["$day_work", 3] }, then: "Wednesday" },
//                                 { case: { $eq: ["$day_work", 4] }, then: "Thrusday" },
//                                 { case: { $eq: ["$day_work", 5] }, then: "Friday" }
//                             ],
//                             default: "Saturday"
//                         }
//                     }
//                 }
//             },
//         ]);
//         return weekSelect;
//     }
//     public async createWeek() {
//         const week = await this.modelWeek.insertMany([
//             { day_work: 0, year: 1941},
//             { day_work: 1, year: 1927},
//             { day_work: 2, year: 1955},
//             { day_work: 3, year: 1890},
//             { day_work: 4, year: 1932},
//             { day_work: 5, year: 1944},
//             { day_work: 6, year: 1916},
//             { day_work: 6, year: 1930},
//         ]);
//         return week;
//     }
//     public async facet() {
//         const listData = await this.modelWeek.aggregate([
//             {
//                 $project: {
//                     _id: true,
//                     day_of_week: {
//                         $switch: {
//                             branches: [
//                                 { case: { $eq: ["$day_work", 0] }, then: "Sunday" },
//                                 { case: { $eq: ["$day_work", 1] }, then: "Monday" },
//                                 { case: { $eq: ["$day_work", 2] }, then: "Tuesday" },
//                                 { case: { $eq: ["$day_work", 3] }, then: "Wednesday" },
//                                 { case: { $eq: ["$day_work", 4] }, then: "Thrusday" },
//                                 { case: { $eq: ["$day_work", 5] }, then: "Friday" }
//                             ],
//                             default: "Saturday"
//                         }
//                     }
//                 }
//             },
//             {
//                 $facet: {
//                     total_sunday: [
//                         {
//                             $match: {
//                                 day_of_week: 'Sunday'
//                             }
//                         },
//                         {
//                             $project: {
//                                 _id: true,
//                                 day_of_week: true,
//                             }
//                         }
//                     ],
//                     total_monday: [
//                         {
//                             $match: {
//                                 day_of_week: 'Monday'
//                             }
//                         },
//                         {
//                             $project: {
//                                 _id: true,
//                                 day_of_week: true,
//                             }
//                         }
//                     ]
//                 }
//             }
//         ]);
//         return listData;
//     }
//     public async bucket() {
//         return await this.modelWeek.aggregate([
//             {
//                 $bucket: {
//                     groupBy: "$year",
//                     boundaries: [ 1840, 1930 ],
//                     default: "Other",
//                     output: {
//                         "count": { $sum: 1 },
//                         data_day_work: {
//                             $push: {
//                                 _id: "$_id",
//                                 year: "$year"
//                             }
//                         }
//                     }
//                 }
//             }
//         ]);
//     }
//     public async addFields() {
//         return await this.model.aggregate([
//             {
//                 $addFields: {
//                     email_name: {
//                         $concat: [
//                             "$username", "-", "$email"
//                         ]
//                     }
//                 }
//             }
//         ]);
//     }
//     public async graphLookup() {
//         // conllection : airports
//         // { "_id" : 0, "airport" : "JFK", "connects" : [ "BOS", "ORD" ] }
//         // { "_id" : 1, "airport" : "BOS", "connects" : [ "JFK", "PWM" ] }
//         // { "_id" : 2, "airport" : "ORD", "connects" : [ "JFK" ] }
//         // { "_id" : 3, "airport" : "PWM", "connects" : [ "BOS", "LHR" ] }
//         // { "_id" : 4, "airport" : "LHR", "connects" : [ "PWM" ] }
//         // traveler
//         // { "_id" : 1, "name" : "Dev", "nearestAirport" : "JFK" }
//         // { "_id" : 2, "name" : "Eliot", "nearestAirport" : "JFK" }
//         // { "_id" : 3, "name" : "Jeff", "nearestAirport" : "BOS" }
//         // query
//         // db.travelers.aggregate( [
//         //     {
//         //        $graphLookup: {
//         //           from: "airports",
//         //           startWith: "$nearestAirport",
//         //           connectFromField: "connects",
//         //           connectToField: "airport",
//         //           maxDepth: 2,
//         //           depthField: "numConnections",
//         //           as: "destinations"
//         //        }
//         //     }
//         //  ] )
//         // note start as : startWith -> connectToField then move to connectFromField then connectFromField move connectToField
//         // example: :-)
//         return await this.model.aggregate([
//             {
//                 $graphLookup: {
//                     from: "work",
//                     startWith: "$_work",
//                     connectFromField: "_id",
//                     connectToField: "_id",
//                     restrictSearchWithMatch: {
//                         name_work: "nodejs"
//                     },
//                     as: "demo"
//                 }
//             },
//             {
//                 $project: {
//                     username: true,
//                     password: true,
//                     email: true,
//                     demo: true
//                 }
//             },
//             {
//                 $unwind: {
//                     path: "$demo",
//                     preserveNullAndEmptyArrays: true
//                 }
//             },
//             {
//                 $addFields: {
//                     dimensions: { $objectToArray: "$demo" }
//                 }
//             },
//             {
//                 $addFields: {
//                     dimensions: {
//                         $arrayToObject: "$dimensions"
//                     }
//                 }
//             }
//         ]);
//     }
//     public async createDate() {
//         return await this.modelDate.insertMany([
//             { date: Date.now() }
//         ])
//     }
//     public async listDate() {
//         return await this.modelDate.aggregate([
//             {
//                 $project: {
//                     year: { $year: "$date" },
//                     month: { $month: "$date" },
//                     day: { $dayOfMonth: "$date" },
//                     hour: { $hour: "$date" },
//                     minutes: { $minute: "$date" },
//                     seconds: { $second: "$date" },
//                     milliseconds: { $millisecond: "$date" },
//                     dayOfYear: { $dayOfYear: "$date" },
//                     dayOfWeek: { $dayOfWeek: "$date" },
//                     week: { $week: "$date" },
//                     dateToString: {
//                         yearMonthDayUTC: { 
//                             $dateToString: { 
//                                 format: "%Y-%m-%d %H:%M:%S",
//                                 date: "$date",
//                                 onNull: "$date" // nếu khi convert date null thì sẽ chạy vô đây
//                             } 
//                         },
//                         timewithOffsetNY: { $dateToString: { format: "%H:%M:%S:%L%z", date: "$date", timezone: "America/New_York"} },
//                         timewithOffset430: { $dateToString: { format: "%H:%M:%S:%L%z", date: "$date", timezone: "+04:30" } },
//                         minutesOffsetNY: { $dateToString: { format: "%Z", date: "$date", timezone: "America/New_York" } },
//                         minutesOffset430: { $dateToString: { format: "%Z", date: "$date", timezone: "+04:30" } },
//                     },
//                     dateFromString: {
//                         date: {
//                             $dateFromString: {
//                                 dateString: "2017-02-08T12:10:40.787",
//                                 timezone: 'America/New_York',
//                                 onError: '$date', // nếu data error khi convert thì sẽ trả về trạng thái bạn muốn onError: 'option'
//                                 onNull: Date.now() // nếu khi convert data null thì sẽ chạy vô đây
//                             }
//                         }
//                     },
//                     dateToParts: {
//                         // {
//                         //     date: input,
//                         //     timezone: America/New_York,
//                         //     iso8601: // If set to true, modifies the output document to use ISO week date fields. Defaults to false.
//                         // }
//                         // nycHour: { $hour: { date: "$date" } },
//                         // nycMinute: { $minute: { date: "$date" } },
//                         // gmtHour: { $hour: { date: "$date" } },
//                         // gmtMinute: { $minute: { date: "$date"} },
//                         // nycOlsonHour: { $hour: { date: "$date"} },
//                         // nycOlsonMinute: { $minute: { date: "$date" } }
//                         $dateToParts: { date: "$date" }
//                     },
//                     dateFromParts: {
//                         $dateFromParts: {
//                             year: 2020,
//                             month: 12,
//                             day: 11,
//                             hour: 10,
//                             minute: 9,
//                             second: 8,
//                             millisecond: 7,
//                             // timezone: America/New_York
//                             // isoDayOfWeek: Day of week Value range: 1-7( Monday 1 - Sunday 7 )
//                             // isoWeek: Week of year. Value range: 1-53
//                             // isoWeekYear: ISO Week Date Year Value range: 1-9999
//                         }
//                     }
//                 }
//             }
//         ]);
//     }
//     public async searchConditions(data: types.Users.Main) {
//         // $ne !=
//         // $or or
//         // $gt => { age: { $gt: 25 } } => WHERE age > 25
//         // $lt => { age: { $lt: 25 } } => WHERE age < 25
//         // both { age: { $gt: 25, $lte: 50 } }
//         // like "%bc%" =>  user_id: /bc/ or $regex: bc
//         // like "bc%" =>  { user_id: /^bc/ }
//         // sort ASC => .sort( { user_id: 1 } )
//         // sort DESC => .sort( { user_id: -1 } )
//         // DISTINCT => this.model.find().distinct( "status" )
//         // EXPLAIN => this.model.find().explain()
//         // { $set: { status: "C" } }, => SET status = "C" using for update ..... learn    { multi: true }
//         const findOptions: db.FilterQuery<types.Users.Main> = {
//         }
//         if (data.username) {
//             findOptions.username = {
//                 $regex: data.username
//             }
//         }
//         if (data.email) {
//             findOptions.email = data.email;
//         }
//         if (data.password) {
//             findOptions.password = data.password;
//         }
//         return await this.model.find(findOptions).select('_id username password email').sort({
//             _id: 1
//         });
//     }
//     public async populate() {
//         // this.model.discriminator('');
//         return await this.model.findOne().populate({
//             // perDocumentLimit: 2 //giới hạn trong mỗi document 
//             // refPath
//             //  add  refPath: 'onModel' in Schema
//             // onModel: {
//             //     type: String,
//             //     required: true,                        => add to Schema
//             //     enum: ['BlogPost', 'Product']
//             //   }
//             path: "_work",
//             match: { 
//                 deleted_at: null
//             },
//             // options: {
//             //     limit: 2     => limit
//             // },
//             populate: {
//                 path: '_type_work',
//                 select: "_id name_type",
//                 match: { 
//                     deleted_at: null
//                 },
//             },
//             select: "_id name_work"
//         }).select({
//             _id: true,
//             username: true,
//             password: true,
//             email: true,
//             created_at: true,
//             updated_at: true
//         });
//     }
// }
// // $setIntersection nấy ra những gì 2 mảng có data giống nhau :
// // example: { $setIntersection: [ [ "a", "b", "a" ], [ "b", "a" ] ] } result: [ "b", "a" ]
// // { $split: [ "June-15-2013", "-" ] } result [ "June", "15", "2013" ] tách chuỗi thành mảng
// // { $slice: [ [ 1, 2, 3 ], 1, 1 ] } result [ 2 ] cắt mảng
// // { $size: <expression> } đếm số lượng data trong mảng
// // { $setUnion: [ [ "a", "b", "a" ], [ "b", "a" ] ] } result [ "b", "a" ] -> lối 2 chuỗi or more thành 1 chuỗi nếu data chùng nhau thì k lối
// // { $setIsSubset: [ [ "a", "b", "a" ], [ "b", "a" ] ] } result :true // gồm 2 mảng nếu mảng đầu là tập con của mảng 2 thì true else false   ////// { $setIsSubset: [ [ "a", "b" ], [ [ "a", "b" ] ] ] } result: false
// // { $setEquals: [ [ "a", "b", "a" ], [ "b", "a" ] ] }	result: true // nều 2 mảng có data giống nhau return true else false// [ "red", "blue" ] with  [ "red", "blue", "green" ] is false
// // { $setDifference: [ [ "a", "b", "a" ], [ "b", "a" ] ] } result [ ] // only return about value exist in the first array { $setDifference: [ [ "green", "red" ], [ "red", "blue" ] ] } result :  [ "green" ]
// // { $rtrim: { input: " ggggoodbyeeeee   ", chars: "e " } }	result: " ggggoodby" nếu bỏ chars sẽ xóa khảng trắng
// // { $replaceAll: { input: "abc", find: "abc", replacement: "bbb" } }	result: bbb
// // { $replaceOne: { input: "abc abc", find: "abc", replacement: "bb" } } result: bb abc
// // { $concatArrays: [[ "hello", " "], [ "world" ]] } result: [ "hello", " ", "world" ]
// // $arrayToObject <> $objectToArray
// //
// // $concat: [
// //     "$username", "-", "$email",
// //     {
// //         $cond: {
// //             if: { $eq: ["$email", "aaaaaaa@gmail.vvv"] },
// //             then: "email not good",
// //             else: "email good"
// //         }
// //     }, "-nothing"
// // ]
//# sourceMappingURL=users.js.map