"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const _base_1 = require("./_base");
class BlogRepository extends _base_1.default {
    constructor(table) {
        super(table);
        this.model = table.Blog;
    }
    findOne() {
        return __awaiter(this, void 0, void 0, function* () {
            const blog = yield this.model.findOne({ title: 'name' }).select('title content type id');
            return blog;
        });
    }
    create(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const blog = yield this.model.create(params);
            return blog;
        });
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const blogs = yield this.model.find({}).select('title content type id');
            return blogs;
        });
    }
}
exports.default = BlogRepository;
//# sourceMappingURL=Blog.js.map