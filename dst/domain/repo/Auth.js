"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const _base_1 = require("./_base");
const jwt = require("jsonwebtoken");
const graphql_1 = require("graphql");
const mongoose = require("mongoose");
const moment = require("moment-timezone");
const utils_1 = require("../utils");
class BlogRepository extends _base_1.default {
    constructor(table) {
        super(table);
        this.model = table.User;
    }
    login(params, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const getUser = yield this.model.findOne({
                email: params.email,
                password: params.password
            });
            if (!getUser) {
                throw new graphql_1.GraphQLError('Password or email is not correct');
            }
            const token = yield this.renderToken(getUser.id);
            const expire = utils_1.convertTimeToVN(moment(new Date()).add(30, 'minutes').toDate());
            yield getUser.updateOne({
                token,
                expire: expire
            });
            res.setHeader('authorization', token);
            const data = {
                id: getUser.id,
                token: token,
                email: getUser.email,
                name: getUser.name,
                expire: getUser.expire,
                created_at: getUser.created_at,
                created_by: getUser.created_by,
                updated_by: getUser.created_at,
                updated_at: getUser.updated_at
            };
            return data;
        });
    }
    findByToken(token) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.model.findOne({
                token,
                expire: {
                    $gte: new Date()
                }
            }).select('email name token expire created_at created_by updated_at updated_by');
            let data = null;
            if (user) {
                data = {
                    name: user.name,
                    email: user.email,
                    expire: user.expire,
                    created_at: user.created_at,
                    created_by: user.created_by,
                    updated_at: user.updated_at,
                    updated_by: user.updated_by,
                    token: user.token
                };
            }
            return data;
        });
    }
    create(params) {
        return __awaiter(this, void 0, void 0, function* () {
            const expire = utils_1.convertTimeToVN(new Date());
            const user = yield this.model.create(Object.assign(Object.assign({}, params), { expire: expire }));
            yield user.updateOne({
                token: yield this.renderToken(user.id),
                created_by: mongoose.Types.ObjectId.createFromHexString(user.id),
                updated_by: mongoose.Types.ObjectId.createFromHexString(user.id),
            });
            const data = {
                id: user.id,
                email: user.email,
                name: user.name,
                created_at: user.created_at,
                created_by: user.id,
                updated_by: user.updated_at,
                updated_at: user.id
            };
            return data;
        });
    }
    renderToken(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const token = yield jwt.sign({ data: id }, 'privateKey');
            return token;
        });
    }
}
exports.default = BlogRepository;
//# sourceMappingURL=Auth.js.map