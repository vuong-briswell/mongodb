"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.convertTimeToVN = void 0;
const moment = require("moment-timezone");
const convertTimeToVN = (date) => {
    let data = null;
    if (date && moment(date).isValid()) {
        data = moment.tz(date, 'Asia/Ho_Chi_Minh').toDate();
    }
    return data;
};
exports.convertTimeToVN = convertTimeToVN;
//# sourceMappingURL=index.js.map