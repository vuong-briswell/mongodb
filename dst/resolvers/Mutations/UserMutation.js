"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createUser = exports.login = void 0;
const validation_1 = require("../../validation");
const login = (args, context) => __awaiter(void 0, void 0, void 0, function* () {
    const input = { email: args.input.email, password: args.input.password };
    yield validation_1.schemaUser.schemaLogin.validate(input, { abortEarly: false });
    return context.db.Auth.login(input, context.res);
});
exports.login = login;
const createUser = (args, context) => __awaiter(void 0, void 0, void 0, function* () {
    const input = { email: args.input.email, name: args.input.name, password: args.input.password };
    yield validation_1.schemaUser.schemaCreate.validate(input, { abortEarly: false });
    return context.db.Auth.create(input);
});
exports.createUser = createUser;
//# sourceMappingURL=UserMutation.js.map