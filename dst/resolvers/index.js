"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolvers = exports.nextWrapper = exports.authenticate = void 0;
const apollo_server_express_1 = require("apollo-server-express");
const BlogQuery_1 = require("./queris/BlogQuery");
const BlogMutation_1 = require("./Mutations/BlogMutation");
const UserMutation_1 = require("./Mutations/UserMutation");
const UserMutation_2 = require("./Mutations/UserMutation");
const graphql_1 = require("graphql");
const requiredAuth = {
    blogs: 'blogs'
};
const notRequiredAuth = {
    login: 'login',
    createUser: 'createUser'
};
const authenticate = (next) => (_, args, context, info) => __awaiter(void 0, void 0, void 0, function* () {
    if (notRequiredAuth.hasOwnProperty(info.fieldName)) {
        return exports.nextWrapper(next)(args, context);
    }
    else if (requiredAuth.hasOwnProperty(info.fieldName)) {
        const token = context.req.headers.authorization || '';
        const user = yield context.db.Auth.findByToken(token);
        if (!user) {
            throw new apollo_server_express_1.AuthenticationError('Authentication token is invalid, please log in');
        }
        context.infoUser = user;
        return exports.nextWrapper(next)(args, context);
    }
    throw new graphql_1.GraphQLError('An unknown error, in src/resolvers/index.ts line 31 noticed');
});
exports.authenticate = authenticate;
const nextWrapper = (mainFunction) => (args, context) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield mainFunction(args, context);
    }
    catch (err) {
        throw new graphql_1.GraphQLError(err);
    }
});
exports.nextWrapper = nextWrapper;
const Query = {
    blogs: exports.authenticate(BlogQuery_1.blogs)
};
const Mutation = {
    createBlog: BlogMutation_1.createBlog,
    login: exports.authenticate(UserMutation_2.login),
    createUser: exports.authenticate(UserMutation_1.createUser),
};
exports.resolvers = {
    Query,
    Mutation,
};
//# sourceMappingURL=index.js.map