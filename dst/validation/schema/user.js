"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.schemaCreate = exports.schemaLogin = void 0;
const common_1 = require("../common");
const factory_1 = require("../../factory");
exports.schemaLogin = common_1.default.object().shape({
    email: common_1.default
        .string()
        .required(factory_1.valueStatic.validationMessages['001']('Email'))
        .max(100, factory_1.valueStatic.validationMessages['002']('Email'))
        .email(factory_1.valueStatic.validationMessages['004']('Email')),
    password: common_1.default
        .string()
        .required(factory_1.valueStatic.validationMessages['001']('Password'))
        .max(100, factory_1.valueStatic.validationMessages['002']('Password'))
});
exports.schemaCreate = common_1.default.object().shape({
    password: common_1.default
        .string()
        .required(factory_1.valueStatic.validationMessages['001']('Password'))
        .max(100, factory_1.valueStatic.validationMessages['002']('Password')),
    email: common_1.default
        .string()
        .required(factory_1.valueStatic.validationMessages['001']('Email'))
        .max(100, factory_1.valueStatic.validationMessages['002']('Email'))
        .email(factory_1.valueStatic.validationMessages['004']('Email')),
    name: common_1.default
        .string()
        .required(factory_1.valueStatic.validationMessages['001']('Name'))
        .max(100, factory_1.valueStatic.validationMessages['002']('Name')),
});
//# sourceMappingURL=user.js.map