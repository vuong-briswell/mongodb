"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const yup = require("yup");
yup.addMethod(yup.date, 'format', function () {
    return this.transform(function (value) {
        if (this.isType(value))
            return value;
        return value.isValid() ? value.toDate() : new Date('');
    });
});
exports.default = yup;
//# sourceMappingURL=common.js.map