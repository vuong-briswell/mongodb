"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createApp = void 0;
const express = require("express");
const path = require("path");
const fs_1 = require("fs");
// import  router from './routes';
// import { MongoClient } from "mongodb";
// import { createDbConnection } from '../mongoDB';
const apollo_server_express_1 = require("apollo-server-express");
const mongoDB_1 = require("./mongoDB");
const resolvers_1 = require("./resolvers");
const domain_1 = require("./domain");
const cors = {
    origin: '*',
    allowedHeaders: [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'X-Access-Token',
        'Authorization',
    ],
    methods: ['GET', 'HEAD', 'OPTIONS', 'PUT', 'PATCH', 'POST', 'DELETE'],
    optionsSuccessStatus: 200,
    credentials: true,
    preflightContinue: false,
};
let dbConnection;
function createApp() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            console.log("DB connecting...");
            dbConnection = yield mongoDB_1.createDbConnection();
            console.log("DB connected");
            const schemaPath = path.join(__dirname, "..", "/schema/output.gql");
            const schemaStr = fs_1.readFileSync(schemaPath, "utf8");
            const typeDefs = apollo_server_express_1.gql `
       ${schemaStr} 
    `;
            const app = express();
            const server = new apollo_server_express_1.ApolloServer({
                typeDefs,
                resolvers: resolvers_1.resolvers,
                formatError: error => {
                    console.log(error);
                    const err = {
                        message: error.message,
                        locations: error.locations,
                        path: error.path,
                    };
                    return err;
                },
                context: ({ req, res, }) => __awaiter(this, void 0, void 0, function* () {
                    res.setHeader("Cache-Control", "no-store");
                    const db = domain_1.repository(dbConnection);
                    return {
                        req,
                        res,
                        db
                    };
                })
            });
            server.applyMiddleware({ app, cors, path: '/graphql' });
            return app;
        }
        catch (err) {
            throw err;
        }
    });
}
exports.createApp = createApp;
//# sourceMappingURL=app.js.map