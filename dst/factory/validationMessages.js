"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validationMessages = void 0;
exports.validationMessages = {
    '001': ($0) => `${$0} is required.`,
    '002': ($0) => `${$0} is too long.`,
    '003': ($0) => `Please enter only a numeric ${$0}.`,
    '004': ($0) => `${$0} is not a valid email address.`
};
//# sourceMappingURL=validationMessages.js.map