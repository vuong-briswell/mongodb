import * as express from 'express';
import * as path from "path";
import { readFileSync } from "fs";
// import  router from './routes';
// import { MongoClient } from "mongodb";
// import { createDbConnection } from '../mongoDB';
import { ApolloServer, gql, CorsOptions } from 'apollo-server-express';
import { createDbConnection } from './mongoDB';
import { Table } from './domain/model';
import { resolvers } from './resolvers';
import { repository } from './domain';
import { ValidationError } from 'yup';
import { SourceLocation, ASTNode, Source } from 'graphql';

type Maybe<T> = null | undefined | T;
interface customError {
  message: string | ValidationError;
  readonly locations: ReadonlyArray<SourceLocation> | undefined;
  readonly path: ReadonlyArray<string | number> | undefined;
  readonly nodes: ReadonlyArray<ASTNode> | undefined;
  readonly source: Source | undefined;
  readonly positions: ReadonlyArray<number> | undefined;
  readonly originalError: Maybe<Error>;
  readonly extensions: { [key: string]: any } | undefined;
}

const cors: CorsOptions = {
  origin: '*',
  allowedHeaders: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'X-Access-Token',
    'Authorization',
  ],
  methods: ['GET', 'HEAD', 'OPTIONS', 'PUT', 'PATCH', 'POST', 'DELETE'],
  optionsSuccessStatus: 200,
  credentials: true,
  preflightContinue: false,
};

let dbConnection: Table | undefined;
export async function createApp() {
  try {
    console.log("DB connecting...");
    dbConnection = await createDbConnection();
    console.log("DB connected");
  
    const schemaPath = path.join(
      __dirname,
      "..",
      "/schema/output.gql"
    );
    const schemaStr = readFileSync(schemaPath, "utf8");
    const typeDefs = gql`
       ${schemaStr} 
    `;
    const app = express();
    const server = new ApolloServer({
      typeDefs,
      resolvers,
      formatError: error => {
        console.log(error);
        const err = {
          message: error.message,
          locations: error.locations,
          path: error.path,
        }
        return err;
      },
      context: async ({
        req,
        res,
      }) => {
        res.setHeader("Cache-Control", "no-store");
        const db = repository(dbConnection!);
        return {
          req,
          res,
          db
        }
      }
    });
    server.applyMiddleware({ app, cors, path: '/graphql' });
    return app;
  } catch(err) {
    throw err;
  }
}