import { connect } from 'mongoose';
import { initialize } from './domain/model';
export const createDbConnection = async () => {
    const uri = "mongodb://localhost:27017/ecommerce";
    const db = await connect(uri, {
        useUnifiedTopology: true,
        useNewUrlParser: true
    });
    return initialize(db);
}