import { Express } from "express";
import { createApp } from './app';
import './mongoDB';

const PORT = process.env.PORT || 3000;
let appCache: Express;
const getServer = async (): Promise<Express> => {
  if (appCache) {
    return appCache;
  }
  const app = await createApp();
  appCache = app;
  return app;
};

getServer()
  .then((app) => app.listen(PORT))
  .then(() => {
    console.log(`Server listening on :${PORT}`);
  })
  .catch(() => {
    process.exit(1);
  });