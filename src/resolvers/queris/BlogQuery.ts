import { repType } from '../../domain/repository';
import { types } from '../../factory';

export const blogs = async (
  _args: never,
  context: types.Common.Context<repType>
) => context.db.Blog.findAll();;

