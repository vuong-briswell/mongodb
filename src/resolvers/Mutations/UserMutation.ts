import { repType } from '../../domain/repository';
import { types } from '../../factory';
import { schemaUser } from '../../validation';

export const login = async (
  args: types.User.ILoginInput,
  context: types.Common.Context<repType>
) => {
  const input = { email: args.input.email, password: args.input.password };
  await schemaUser.schemaLogin.validate(input, { abortEarly: false });
  return context.db.Auth.login(input, context.res);
};

export const createUser = async (
  args: types.User.ICreateUserInput,
  context: types.Common.Context<repType>
) => {
  const input = { email: args.input.email, name: args.input.name, password: args.input.password };
  await schemaUser.schemaCreate.validate(input, { abortEarly: false })
  return context.db.Auth.create(input);
};