import { repType } from '../../domain/repository';
import { types } from '../../factory';

export const createBlog = async (
  args: types.Blog.CreateBlog,
  context: types.Common.Context<repType>
) => context.db.Blog.create(args);
