import { IResolvers, AuthenticationError } from "apollo-server-express";
import { blogs } from './queris/BlogQuery';
import { createBlog } from './Mutations/BlogMutation';
import { createUser } from './Mutations/UserMutation';
import { login } from './Mutations/UserMutation';
import { repType } from '../domain/repository';
import { types } from "../factory";
import { GraphQLResolveInfo, GraphQLError } from 'graphql';

const requiredAuth = {
    blogs: 'blogs'
}

const notRequiredAuth = {
    login: 'login',
    createUser: 'createUser'
}

export const authenticate = (next: Function) => async (_: never, args: any, context: types.Common.Context<repType> , info: GraphQLResolveInfo) => {
    if (notRequiredAuth.hasOwnProperty(info.fieldName)) {
        return nextWrapper(next)(args, context);
    } else if (requiredAuth.hasOwnProperty(info.fieldName)) {
        const token = context.req.headers.authorization || '';
        const user = await context.db.Auth.findByToken(token);
        if (!user) {
            throw new AuthenticationError('Authentication token is invalid, please log in');
        }
        context.infoUser = user!;
        return nextWrapper(next)(args, context);
    }
    throw new GraphQLError('An unknown error, in src/resolvers/index.ts line 31 noticed');
};

export const nextWrapper = (
    mainFunction: Function
  ) => async (args: any, context: types.Common.Context<repType>) => {
    try {
        return await mainFunction(args, context);
    } catch(err) {
        throw new GraphQLError(err);
    }
}

const Query = {
    blogs: authenticate(blogs)
};

const Mutation = {
    createBlog,
    login: authenticate(login),
    createUser: authenticate(createUser),
}

export const resolvers = {
    Query,
    Mutation,
} as IResolvers<any, any>;