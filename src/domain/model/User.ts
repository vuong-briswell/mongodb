import { Schema, Document, Mongoose } from 'mongoose';
import { types } from '../../factory';
import { timestamp } from './_common'

export const User = (mongoose: Mongoose) => {
    const UserSchema = new mongoose.Schema({
        email: {
            type: Schema.Types.String,
            required: true
        },
        name: {
            type: Schema.Types.String,
            required: true
        },
        password: {
            type: Schema.Types.String,
        },
        token: {
            type: Schema.Types.String,
        },
        expire: {
            type: Schema.Types.Date,
            required: true
        }
    }, {
        toJSON: {
            virtuals: true
        },
        id: true,
        versionKey: false
    }).plugin(timestamp);
    
    return mongoose.model<Document & types.User.Main>('user', UserSchema, 'user');
}