import { Schema, Document, Mongoose } from 'mongoose';
import { types } from '../../factory';
import { timestamp } from './_common'

export const Blog = (mongoose: Mongoose) => {
    const BlogSchema = new mongoose.Schema({
        // customerId: {
        //     type: Schema.Types.ObjectId,
        //     ref: 'Customer',
        //     required: true
        // },
        title: {
            type: Schema.Types.String,
            // ref: 'Restaurant',
            // required: true
        },
        content: {
            type: Schema.Types.String,
            // ref: 'Order',
            // order: true
        },
        type: {
            type: String,
            enum: ['php', 'nodejs', 'reactjs'],
        }
    }, {
        toJSON: {
            virtuals: true
        },
        id: true,
        versionKey: false
    }).plugin(timestamp);
    
    return mongoose.model<Document & types.Blog.Main>('blog', BlogSchema, 'blog');
}