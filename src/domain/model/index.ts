// import User from './users';
import { Mongoose } from 'mongoose';
// import  Work from './work';
// import TypeWork from './TypeBlog';
// import Week from './week';
// import Date from './date';
import { Blog } from './Blog';
import { User } from './User';
export type Table = ReturnType<typeof initialize>;

export const initialize = (mongoose: Mongoose) => {
    const model = {
        Blog: Blog(mongoose),
        User: User(mongoose)
    }

    return {
        ...model,
        mongoose
    }
}