import BaseRepository from './_base';
import { Table } from '../model';
import { types } from '../../factory';

export interface IBlogRepository {
  findOne(): Promise<{ title: string, content: string , type: string, id: number | string} | null>
}
export default class BlogRepository extends BaseRepository implements IBlogRepository {
  private readonly model: Table['Blog'];

  constructor(table: Table) {
    super(table);
    this.model = table.Blog;
  }

  public async findOne() {
    const blog = await this.model.findOne({title: 'name'}).select('title content type id');
    return blog;
  }

  public async create(params: types.Blog.CreateBlog) {
    const blog = await this.model.create(params);
    return blog;
  }
  public async findAll() {
    const blogs = await this.model.find({}).select('title content type id');
    return blogs;
  }
}