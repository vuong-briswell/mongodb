import { Table } from '../model';

export default abstract class BaseRepository {
    public readonly db: Table;
    
    constructor(table: Table) {
        this.db = table;
    }
}