import BaseRepository from './_base';
import { Table } from '../model';
import { types, graphqlType } from '../../factory';
import * as jwt from 'jsonwebtoken';
import { GraphQLError } from 'graphql';
import * as mongoose from 'mongoose';
import * as moment from 'moment-timezone';
import { ExpressContext } from 'apollo-server-express';
import { convertTimeToVN } from '../utils';

export default class BlogRepository extends BaseRepository implements types.User.IAuthRepository {
  private readonly model: Table['User'];

  constructor(table: Table) {
    super(table);
    this.model = table.User;
  }

  public async login(params: graphqlType.LoginInput, res: ExpressContext['res']) {
    const getUser = await this.model.findOne({
      email: params.email,
      password: params.password
    });
    if (!getUser) {
      throw new GraphQLError('Password or email is not correct');
    }
    const token = await this.renderToken(getUser.id);
    const expire = convertTimeToVN(moment(new Date()).add(30, 'minutes').toDate());
    await getUser.updateOne({
      token,
      expire: expire!
    });
    res.setHeader('authorization', token);
    const data = <graphqlType.Login>{
      id: getUser.id,
      token: token,
      email: getUser.email,
      name: getUser.name,
      expire: getUser.expire,
      created_at: getUser.created_at,
      created_by: getUser.created_by,
      updated_by: getUser.created_at,
      updated_at: getUser.updated_at
    }
    return data;
  }

  public async findByToken(token: string) {
    const user = await this.model.findOne({
      token,
      expire: {
        $gte: new Date()
      }
    }).select('email name token expire created_at created_by updated_at updated_by');
    let data = null;
    if (user) {
      data = <types.User.Main> {
        name: user.name,
        email: user.email,
        expire: user.expire,
        created_at: user.created_at,
        created_by: user.created_by,
        updated_at: user.updated_at,
        updated_by: user.updated_by,
        token: user.token
      }
    }
    return data;
  }

  public async create(params: graphqlType.CreateUserInput) {
    const expire = convertTimeToVN(new Date());
    const user = await this.model.create({
      ...params,
      expire: expire!
    });
    await user.updateOne({
      token: await this.renderToken(user.id),
      created_by: mongoose.Types.ObjectId.createFromHexString(user.id),
      updated_by: mongoose.Types.ObjectId.createFromHexString(user.id),
    });
    const data = {
      id: user.id,
      email: user!.email,
      name: user!.name,
      created_at: user.created_at,
      created_by: user.id,
      updated_by: user.updated_at,
      updated_at: user.id
    };
    return data;
  }

  private async renderToken (id: String) {
    const token = await jwt.sign({ data: id }, 'privateKey');
    return token;
  }
}