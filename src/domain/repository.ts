import BlogRepository from './repo/Blog';
import AuthRepository from './repo/Auth';
import { Table } from './model';

export class Blogs extends BlogRepository {}

export type repType = ReturnType<typeof reposetory>;

export const reposetory = (dbConnection: Table) => {
    return {
        Blog: new BlogRepository(dbConnection),
        Auth: new AuthRepository(dbConnection)
    }
}