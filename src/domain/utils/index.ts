import * as moment from 'moment-timezone';

export const convertTimeToVN = (date?: Date) => {
    let data = null;
    if (date && moment(date).isValid()) {
        data = moment.tz(date, 'Asia/Ho_Chi_Minh').toDate();
    }
    return data;
}