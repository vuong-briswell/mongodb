import yup from '../common';

import { valueStatic } from '../../factory';
export const schemaLogin = yup.object().shape({
  email: yup
          .string()
          .required(valueStatic.validationMessages['001']('Email'))
          .max(100, valueStatic.validationMessages['002']('Email'))
          .email(valueStatic.validationMessages['004']('Email')),
  password: yup
            .string()
            .required(valueStatic.validationMessages['001']('Password'))
            .max(100, valueStatic.validationMessages['002']('Password'))
});

export const schemaCreate = yup.object().shape({
password: yup
          .string()
          .required(valueStatic.validationMessages['001']('Password'))
          .max(100, valueStatic.validationMessages['002']('Password')),
  email: yup
          .string()
          .required(valueStatic.validationMessages['001']('Email'))
          .max(100, valueStatic.validationMessages['002']('Email'))
          .email(valueStatic.validationMessages['004']('Email')),
  name: yup
        .string()
        .required(valueStatic.validationMessages['001']('Name'))
        .max(100, valueStatic.validationMessages['002']('Name')),
})