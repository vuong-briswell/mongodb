import * as yup from 'yup';

yup.addMethod(yup.date, 'format', function () {
    return this.transform(function (value) {
      if (this.isType(value)) return value;
  
      return value.isValid() ? value.toDate() : new Date('');
    });
});

export default yup;
