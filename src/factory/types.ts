import * as CommonFactory from './main/common';
import * as BlogFactory from './main/Blog';
import * as UserFactory from './main/User';

export namespace Blog {
    export import CreateBlog = BlogFactory.createBlog;
    export import Main = BlogFactory.Main;
}
export namespace Common {
    export import FieldCM = CommonFactory.FieldCM;
    export import Context = CommonFactory.Context;
}

export namespace User {
    export import Main = UserFactory.Main;
    export import IAuthRepository = UserFactory.IAuthRepository;
    export import ILoginInput = UserFactory.ILoginInput;
    export import ICreateUserInput = UserFactory.ICreateUserInput;
}