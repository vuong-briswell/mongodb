import { FieldCM } from './common';
import { Login, LoginInput, CreateUserInput } from '../graphql';
import { ExpressContext } from 'apollo-server-express';

export interface Main extends FieldCM {
    email?: string;
    name?: string;
    password?: string;
    token?: string;
    expire?: Date;
}

export interface ILoginInput {
    input: LoginInput
}

export interface ICreateUserInput {
    input: CreateUserInput
}
export interface IAuthRepository {
    findByToken(token: string): Promise<Main | null>;
    login(params: LoginInput, req: ExpressContext['res']): Promise<Login | null>;
}