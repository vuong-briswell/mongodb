export interface createBlog {
    title: string;
    content: string;
    type: string;
}

export interface Main {
    title: string;
    content: string;
    type: string;
}