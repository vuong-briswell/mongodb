import { Types } from 'mongoose';
import { ExpressContext } from 'apollo-server-express';
import { Main } from './User';
export interface FieldCM {
    created_at?: Date,
    created_by?: Types.ObjectId,
    updated_at?: Date,
    updated_by?: Types.ObjectId,
    deleted_at?: Date,
    deleted_by?: Types.ObjectId
}

export interface Context<T> {
    req: ExpressContext['req'];
    res: ExpressContext['res'];
    db: T;
    infoUser?: Main
}