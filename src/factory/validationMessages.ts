export const validationMessages = {
    '001': ($0: string) => `${$0} is required.`,
    '002': ($0: string) => `${$0} is too long.`,
    '003': ($0: string) => `Please enter only a numeric ${$0}.`,
    '004': ($0: string) => `${$0} is not a valid email address.`
}